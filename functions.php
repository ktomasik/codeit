<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 13.08.2018
 * Time: 08:50
 */
if (!session_id()) {
    session_start();
}

add_action( 'wp_enqueue_scripts', 'twentyseventeen_child_scripts' );
function twentyseventeen_child_scripts() {
    wp_enqueue_script('custom', '/wp-content/themes/codeit/custom.js', array(), '1.0.0', true );
    wp_enqueue_style('timeline', '/wp-content/themes/codeit/css/timeline.css', false,'1.1','all');
    wp_enqueue_style('fontAwesome', 'https://use.fontawesome.com/releases/v5.3.1/css/all.css');
    wp_enqueue_style('lesson', '/wp-content/themes/codeit/css/lesson-card.css', false,'1.1','all');
    wp_enqueue_script('lessonPlanForm', '/wp-content/themes/codeit/lessonPlanForm.js', array(), '1.0.0', true );
}

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);

    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }

    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

    return $excerpt;
}

function getSinglePlan($id)
{
    global $wpdb;
    $results = $wpdb->get_row('SELECT * FROM `co_lesson_plans` WHERE `id`='.$id, OBJECT);
    return $results;
}

function getPlanComments($id)
{
    global $wpdb;
    $results = $wpdb->get_results('SELECT `comment` FROM `co_plan_comments` WHERE `plan_id`='.$id, OBJECT);
    return $results;
}