<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 14.11.2018
 * Time: 10:00
 */
require_once('../../../wp-load.php');

$toolId = $_POST['toolId'];
$newDir = '';
$dir = 'lesson-attachments/' . $toolId;

global $wpdb;


/**
 * move attached files
 */

if (!is_dir($dir)) {
    $newDir = mkdir('lesson-attachments/' . $toolId);
} else {
    $newDir = 'lesson-attachments/' . $toolId;
}

$file_ary = reArrayFiles($_FILES['file']);

foreach ($file_ary as $file) {
    $name = $file['name'];

    if (0 < $file['error']) {
        echo 'Error: ' . $file['error'] . '<br>';
    } else {
        move_uploaded_file($file['tmp_name'], 'lesson-attachments/' . $toolId . '/' . $name);
    }
}

function reArrayFiles(&$file_post)
{
    $file_ary = [];
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i = 0; $i < $file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

/**
 * adding new lesson plan to database
 */
    $planTitle = $_POST['title'];
    $time = $_POST['time'];
    $grade = $_POST['grade'];
    $knowledge = serialize($_POST['knowledge']);
    $skills = serialize($_POST['skills']);
    $attitudes = serialize($_POST['attitudes']);
    $methods =serialize($_POST['method']);
    $didactic = serialize($_POST['didactic']);
    $introduction = serialize($_POST['introduction']);
    $coreParts = serialize($_POST['core-part']);
    $summary = serialize($_POST['summary']);
    $subject = $_POST['domain'];
    $appendices = implode(', ', $_FILES['file']['name']);
    $id = $_POST['toolId'];

    $wpdb->insert('co_lesson_plans', array(
        'id' => $id,
        'title' => $planTitle,
        'time' => $time,
        'grade' => $grade,
        'knowledge' => $knowledge,
        'skills' => $skills,
        'attitudes' => $attitudes,
        'methods' => $methods,
        'didactic_materials' => $didactic,
        'introduction' => $introduction,
        'core_parts' => $coreParts,
        'summary' => $summary,
        'subject' => $subject,
        'appendices' => $appendices,
        'like' => 0,
        'dislike' => 0,
        'views' => 0
    ));
