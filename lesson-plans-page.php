<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 18.09.2018
 * Time: 11:53
 * Template name: Lesson plans
 */

if (isset($_GET['plan_id'])) {
    $planId = intval($_GET['plan_id']);
    include 'single-plan.php';
    return;
}

global $wpdb;

$lastToolId = $wpdb->get_row('SELECT MAX(id) FROM `co_lesson_plans`', ARRAY_N);

$allPlans = $wpdb->get_results('SELECT * FROM `co_lesson_plans` ORDER BY `title`', OBJECT);

$toolId = '';
if (!empty($lastToolId[0])) {
    $toolId = $lastToolId[0] + 1;
} else {
    $toolId = 1;
}

get_header();
?>
    <style>
        .btn-danger, .btn-success {
            font-weight: bold;
        }
        ul.subjectFilter li {
            margin: 5px 0;
        }
    </style>
    <div class="container-fluid" id="lesson-plans">
        <div id="primary" class="content-area" style="max-width: 850px;">
            <main id="main" class="site-main" role="main">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <h1 class="text-center" style="margin-bottom: 50px;">Lesson Plans</h1>
                    <div class="row">
                        <div class="col-md-3">
                            <h4>Subjects</h4>
                            <ul class="subjectFilter" style="list-style-type: none;">
                                <li><a href="#" class="btn btn-warning sortSubject" data-value="chemistry">Chemistry</a></li>
                                <li><a href="#" class="btn btn-warning sortSubject" data-value="math">Math</a></li>
                                <li><a href="#" class="btn btn-warning sortSubject" data-value="geography">Geography</a></li>
                                <li><a href="#" class="btn btn-warning sortSubject" data-value="physic">Physic</a></li>
                                <li><a href="#" class="btn btn-warning resetBtn">Reset</a></li>
                            </ul>
                            <?php if (is_user_logged_in()): ?>
                                <button id="addLessonPlan" class="btn btn-info">Add lesson plan</button>
                            <?php endif; ?>
                        </div>

                        <div id="lesson" class="col-md-9">
                            <div class="row">
                                <?php
                                foreach ($allPlans as $plan):?>
                                    <div class="col-md-4 <?= $plan->subject ?> plan-<?= $plan->id ?>">
                                        <a href="/lesson-plans?plan_id=<?= $plan->id ?>" class="id-<?= $plan->id ?>"
                                           id="<?= $plan->id ?>" data-value="<?= $plan->views + 1 ?>"
                                           style="text-decoration: none;">
                                            <div class="lesson-card">
                                                <div class="lesson-card-title text-center">
                                                    <?= $plan->title ?>
                                                </div>
                                                <br/><br/>
                                                <hr style="width: 50%; margin: 5px auto"/>
                                                <div class="row align-items-end">
                                                    <div class="col-md-6">
                                                        <i class="fas fa-eye"
                                                           style="color: #2e6da4;"> <?= $plan->views ?></i>
                                                    </div>
                                                    <div class="col-md-6 text-right">
                                                        <?php
                                                        $commentsNo = '';

                                                        $comments = $wpdb->get_results('SELECT COUNT(plan_id) FROM `co_plan_comments` WHERE `plan_id`=' . $plan->id, ARRAY_A);

                                                        foreach ($comments as $no) {
                                                            foreach ($no as $n) {
                                                                $commentsNo = $n;
                                                            }
                                                        } ?>
                                                        <i class="fas fa-comments"
                                                           style="color: #fd622f;"> <?= $commentsNo ?></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                <?php endforeach; ?>
                            </div>

                        </div>

                        <div id="newLessonPlan" class="col-md-9" style="display: none;">
                            <div class="row">
                                <button id="backToPlansList" class="btn btn-info">◄&nbsp; Back to Plans list</button>
                            </div>
                            <div class="row">
                                <form id="addPlan" method="post" enctype="multipart/form-data" style="width: 100%">
                                    <h3>Basic informations</h3>
                                    <div class="basic-info form-group">
                                        <label>Lesson Plan title<i title="required" style="color:red">*</i>: <input
                                                    type="text" name="title" required/></label>
                                        <label>Domain<i title="required" style="color:red">*</i>: <select name="domain"
                                                                                                          style="width: 100%;"
                                                                                                          required>
                                                <option></option>
                                                <option value="chemistry">Chemistry</option>
                                                <option value="math">Math</option>
                                                <option value="geography">Geography</option>
                                                <option value="physic">Physic</option>
                                            </select></label>
                                        <label>Duration: <input type="time" name="time"/></label>
                                        <label>Grade: <input type="text" name="grade"/></label>
                                    </div>
                                    <hr/>
                                    <h3>Learning Outcomes</h3>
                                    <div class="learningOutcomes form-group">
                                        <div class="row">
                                            <div class="col-md-10 knowledge">
                                                <label for="knowledge[]">Knowledge:</label>
                                                <input type="text" name="knowledge[]" class="input-knowledge"/>
                                            </div>
                                            <div class="col-md-2 align-self-end">
                                                <button id="add-knowledge" class="btn btn-success"
                                                        style="margin-right: 5px;">+
                                                </button>
                                                <button id="remove-knowledge" class="btn btn-danger">-</button>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-10 skills">
                                                <label for="skills[]">Skills: </label>
                                                <input type="text" name="skills[]" class="input-skills"/>
                                            </div>
                                            <div class="col-md-2 align-self-end">
                                                <button id="add-skill" class="btn btn-success"
                                                        style="margin-right: 5px;">+
                                                </button>
                                                <button id="remove-skill" class="btn btn-danger">-</button>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-10 attitudes">
                                                <label for="attitudes[]">Attitudes/Soft skills: </label>
                                                <input type="text" name="attitudes[]" class="input-attitudes"/>
                                            </div>
                                            <div class="col-md-2 align-self-end">
                                                <button id="add-attitude" class="btn btn-success"
                                                        style="margin-right: 5px;">+
                                                </button>
                                                <button id="remove-attitude" class="btn btn-danger">-</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <h3>Methods and types of work</h3>
                                    <div class="methods form-group">
                                        <div class="row">
                                            <div class="col-md-10 method">
                                                <input type="text" name="method[]" class="input-method"/>
                                            </div>
                                            <div class="col-md-2 align-self-end">
                                                <button id="add-method" class="btn btn-success"
                                                        style="margin-right: 5px;">+
                                                </button>
                                                <button id="remove-method" class="btn btn-danger">-</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <h3>Didactic materials/resources</h3>
                                    <div class="didactics form-group">
                                        <div class="row">
                                            <div class="col-md-10 didactic">
                                                <input type="text" name="didactic[]" class="input-didactic"/>
                                            </div>
                                            <div class="col-md-2 align-self-end">
                                                <button id="add-didactic" class="btn btn-success"
                                                        style="margin-right: 5px;">+
                                                </button>
                                                <button id="remove-didactic" class="btn btn-danger">-</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <h3>The course of the lesson</h3>
                                    <div class="course form-group">
                                        <div class="row">
                                            <div class="col-md-10 introduction">
                                                <label for="introduction[]">Introduction:</label>
                                                <input type="text" name="introduction[]" class="input-introduction"/>
                                            </div>
                                            <div class="col-md-2 align-self-end">
                                                <button id="add-introduction" class="btn btn-success"
                                                        style="margin-right: 5px;">+
                                                </button>
                                                <button id="remove-introduction" class="btn btn-danger">-</button>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-10 core-part">
                                                <label for="core-part[]">Core part:</label>
                                                <input type="text" name="core-part[]" class="input-core-part"/>
                                            </div>
                                            <div class="col-md-2 align-self-end">
                                                <button id="add-core-part" class="btn btn-success"
                                                        style="margin-right: 5px;">+
                                                </button>
                                                <button id="remove-core-part" class="btn btn-danger">-</button>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-md-10 summary">
                                                <label for="summary[]">Summary:</label>
                                                <input type="text" name="summary[]" class="input-summary"/>
                                            </div>
                                            <div class="col-md-2 align-self-end">
                                                <button id="add-summary" class="btn btn-success"
                                                        style="margin-right: 5px;">+
                                                </button>
                                                <button id="remove-summary" class="btn btn-danger">-</button>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="toolId" value="<?= $toolId ?>"/>
                                    <hr/>
                                    <h3>Appendices</h3>
                                    <div class="didactics form-group">
                                        <div class="row">
                                            <div class="col-md-10 appendices">
                                                <input name="file[]" type="file" accept=".doc,.pdf, .docx, .txt, .png, .jpg" multiple>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-top: 40px;">
                                        <input id="addLesson" type="submit" class="btn btn-success"
                                               value="Add lesson plan"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </article>

            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .container -->

<?php
get_footer();