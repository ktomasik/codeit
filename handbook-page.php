<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 13.08.2018
 * Time: 08:19
 * Template name: Handbook
 */
$userId = get_current_user_id();
global $wpdb;

add_query_arg('chapter1', $_GET['chapter1']);
add_query_arg('chapter2', $_GET['chapter2']);
add_query_arg('chapter3', $_GET['chapter3']);

$userActions = $wpdb->get_row('SELECT * FROM `co_user_actions` WHERE `user_id`='.$userId);
$testCompleted = $userActions->test_response;

$finishedPosts = get_user_meta($userId, 'progress', true);

$tmpArr = [];
$tmpArr2 = [];
$tmpArr3= [];

if (isset($finishedPosts[1])) {
    foreach ($finishedPosts[1] as $post) {
        $tmpArr[] = $post;
    }
}
if (isset($finishedPosts[2])) {
    foreach ($finishedPosts[2] as $post) {
        $tmpArr2[] = $post;
    }
}
if (isset($finishedPosts[3])) {
    foreach ($finishedPosts[3] as $post) {
        $tmpArr3[] = $post;
    }
}


get_header(); ?>
    <div class="container" id="handbook">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <?php if (!isset($_GET['chapter1']) && !isset($_GET['chapter2']) && !isset($_GET['chapter3'])): ?>
                        <h1 class="handbook-title">Algorithmic and Programming - Training materials for Teachers<br/>Algorithmic.
                            Programming. Didactics.</h1>
                        <div class="row" style="margin-bottom: 30px">
                            <p class="text-justify">
                                Ability to using algorithmic and programming is recognized by the European authorities
                                as one of the important, nowadays skill forming part of “digital competence” which is
                                one from eight key competences. In EURYDICE report (2012)
                                a following statement has been made: “The need to improve the quality and relevance of
                                the skills and competences with which young Europeans leave school has been recognised
                                at EU and national level. The urgency of addressing this
                                issue is further underlined by the current situation in which Europe faces high youth
                                unemployment and, in some cases, serious skills mismatches (…). The European Policy
                                Network on the Implementation of the Key Competences (KeyCoNet)
                                analyses emerging initiatives for the development of the key competences (…). One of
                                them relates to the need for a more strategic approach in supporting the key competences
                                approach at school. A second one is related to the efforts
                                to enhance the status of the transversal competences (digital, civic and
                                entrepreneurship) as compared to the traditional subject-based competences.”
                            </p>
                            <p class="text-justify">
                                Publication entitled: <em>Algorithmic and Programming - Training materials for Teachers.
                                    Algorithmic. Programming. Didactics.</em> meets these recommendations. The main aim
                                of the publication is presenting the teachers of an idea of
                                algorithmic and programming along with their practical application in didactics. The
                                publication is the first Intellectual Output of the project entitled <strong>“CodeIT:
                                    Enhancing Teachers’ professional development through algorithmic
                                    and programming”</strong>, which is realised by the international consortium consist
                                of six partners from five countries: P.T.E.A. Wszechnica Sp. z o.o. (Krosno, Poland),
                                M.K. INNOVATIONS LTD (Nicosia, Cyprus), Danmar Computers Sp. z o.o.
                                (Rzeszów, Poland), Istituto Superiore E. Mattei (Fiorenzuola d’Arda, Italy), Liceul
                                Pedagogic “Mircea Scarlat” Alexandria (Alexandria, Romania) and Kekavas vidusskola
                                (Kekava, Latvia). The project is realised under the frame of
                                Erasmus+, Strategic Partnership Programme.
                            </p>
                            <p class="text-justify">
                                The main aim of the project is to help teachers enhance their professional development
                                by raising programming competences through the development of innovative resources.
                                Primary target group are non-IT teachers from elementary schools
                                (grades 4 and higher) and gymnasiums (lower-secondary schools) with special attention to
                                teachers of Chemistry, Geography, Math and Physics. Secondary target group is students
                                abovementioned schools.
                            </p>
                            <p class="text-justify">
                                The publication consist of three chapters. The first chapter is devoted to the
                                algorithmic and it presents the idea of programs and programming languages, the
                                importance of algorithms and its design. The second chapter presents the
                                definition of programming, its history as well as programmers skills and the process of
                                developing them. In this chapter the information about principles of programming are
                                also consisted. The last one chapter is devoted to
                                presentation of didactic elements with the use of algorithmic and programming. In this
                                chapter the information about basic assumptions of algorithmic and programming in school
                                teaching, as well as computational thinking concept are
                                presented. The final part of the publication is devoted to presentation of practical
                                application of computational thinking.
                            </p>
                            <p class="text-justify">
                                The Authors hope that the publication meets meet with interest from teachers and brings
                                them the useful knowledge in algorithmic and programming. The publication initialized
                                the set of educational materials for teachers and students,
                                which will also include:
                            </p>
                            <ul class="text-justify" style="margin-left: 45px;">
                                <li>Virtual Learning Environment for Teachers containing training materials in
                                    algorithmic and programming and its didactic in other than IT subjects,
                                </li>
                                <li>Model lesson plans incorporating programming for Chemistry, Geography, Maths and
                                    Physics,
                                </li>
                                <li>Handbook entitled “Advance your teaching skills with the use of algorithmic and
                                    programming”.
                                </li>
                            </ul>
                        </div>
                        <?php if(is_user_logged_in()):?>
                        <div class="row justify-content-center" style="margin-bottom: 30px;">
                            <div class="col-md-10 text-center">
                                <a href="/test/?before" class="btn btn-primary btn-test" id="testBefore">Test before</a>
                                <a href="/test/?after" class="btn btn-primary btn-test-after" id="testAfter" style="display: none;">Test after</a>
                            </div>
                        </div>
                        <?php else:?>
                            <div class="row justify-content-center" style="margin-bottom: 30px;">
                                <div class="col-md-10 text-center">
                                    <a href="#" class="btn btn-primary" id="notLogin">Test before</a>
                                </div>
                            </div>
                        <?php endif; ?>


                        <div class="row" id="chapters-buttons" style="display: none">
                            <div class="col-md-4 handbook-tab">
                                <div class="handbook-chapters">
                                    <a href="/handbook/?chapter1">
                                        <img src="/wp-content/uploads/chapter1.jpg" class="img-fluid"/>
                                        <h3 style="text-align: center; font-weight: bold;">Chapter 1 - Introduction to algorithmic</h3>
                                        <p>An algorithm is a description of how a specific problem should be
                                            solved.<br/>
                                            If you have ever baked brownies, you know that first you have to gather
                                            ingredients, then measure them, mix them together then prepare the pan, heat
                                            the oven and cook them.
                                        </p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 handbook-tab">
                                <div class="handbook-chapters">
                                    <a href="/handbook/?chapter2">
                                        <img src="/wp-content/uploads/chapter2.jpg" class="img-fluid"/>
                                        <h3 style="text-align: center; font-weight: bold;">Chapter 2 - Introduction to programming</h3>
                                        <p>The intuitive definition of programming does not cause much difficulty and
                                            brings to mind the introduction of commands in the form of a code, expressed
                                            in a specific programming
                                            language. However, defining the exact essence of programming is a bit more
                                            difficult.
                                        </p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 handbook-tab">
                                <div class="handbook-chapters">
                                    <a href="/handbook/?chapter3">
                                        <img src="/wp-content/uploads/chapter3.jpeg" class="img-fluid"/>
                                        <h3 style="text-align: center; font-weight: bold;">Chapter 3 - Didactics with the use of algorithmic and programming</h3>
                                        <p>Modern school should, among other things, prepare a young person for life in
                                            a changing technological world, at the same time in a world based on the
                                            unchanging laws of nature.
                                            The ability to function in society is also important.
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>



                    <?php elseif (isset($_GET['chapter1'])): ?>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="text-center" style="width: 100%;">
                                    <h1> Chapter 1 - Introduction to algorithmic</h1>
                                </div>
                            </div>
                            <div class="row">
                                <a href="/handbook" class="btn btn-info">◄&nbsp; Back to Main Menu</a>

                                <div class="container">
                                    <?php if (isset($finishedPosts[1])){
                                        $var1 = 100 / 11;
                                        $res = $var1 * (count($finishedPosts[1]));
                                    }
                                    ?>
                                    <h2 class="text-center">Your progress</h2>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:<?= $finishedPosts[1] ? $res : 0?>%">
                                            <span><?=$finishedPosts[1] ? round($res) : 0?>% Complete</span>
                                        </div>
                                    </div>
                                </div>
                                <div id="timeline">
                                    <?php
                                    global $post;
                                    $args = array('category' => '23','posts_per_page' => 11,);
                                    $myposts = get_posts($args);
                                    $i = 0;
                                    foreach ($myposts as $post): setup_postdata($post); ?>
                                        <div class="row timeline-movement post-<?= $i ?>">
                                            <div class="timeline-badge center-left badge-<?= $i ?> <?=get_the_ID();?>"></div>
                                            <div class="col-sm-6 timeline-item" data-target="<?= $i ?>">
                                                <div class="row">
                                                    <div class="col-sm-11">
                                                        <div class="timeline-panel credits anim animate fadeInLeft credits-<?= $i ?>" data-value="1">
                                                            <ul class="timeline-panel-ul">
                                                                <div class="lefting-wrap">
                                                                    <li class="img-wraping"><a href="<?php the_permalink(); ?>/?chapter1" class="clicked-article"><?php the_post_thumbnail('img-responsive'); ?></a>
                                                                    </li>
                                                                </div>
                                                                <div class="righting-wrap">
                                                                    <li><?= the_title('<a href="' . esc_url(get_permalink()) . '/?chapter1" class="importo clicked-article">', '</a>'); ?></li>
                                                                    <li><a href="<?=esc_url(get_permalink())?>/?chapter1" class="post-desc"><?= excerpt(25) ;?></a></li>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="finished text-center" style="display: none;">
                                                                    <i class="fas fa-check-double"></i> Post read
                                                                </div>
                                                            </ul>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $i++;
                                    endforeach;
                                    ?>

                                </div>
                            </div>
                        </div>
                        <script>
                            let tempArr = [<?= '"'.implode('","', $tmpArr).'"' ?>];
                            for (var i = 0; i < tempArr.length; i++ ) {
                                $('.timeline-movement.post-'+tempArr[i]+' .finished').show();
                            }
                        </script>
                    <?php elseif (isset($_GET['chapter2'])): ?>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="text-center" style="width: 100%;">
                                    <h1> Chapter 2 - Introduction to programming</h1>
                                </div>
                            </div>
                            <div class="row">
                                <a href="/handbook" class="btn btn-info">◄&nbsp; Back to Main Menu</a>

                                <div class="container">
                                    <h2 class="text-center">Your progress</h2>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:<?= $finishedPosts[2] ? count($finishedPosts[2])*10 : 0?>%">
                                            <span><?= $finishedPosts[2] ? count($finishedPosts[2])*10 : 0?>% Complete</span>
                                        </div>
                                    </div>
                                </div>

                                <div id="timeline">
                                    <?php
                                    global $post;
                                    $args = array('category' => '28', 'posts_per_page' => 10,);
                                    $myposts = get_posts($args);
                                    $i = 0;
                                    foreach ($myposts as $post): setup_postdata($post); ?>
                                        <div class="row timeline-movement post-<?= $i ?>">
                                            <div class="timeline-badge center-left badge-<?= $i ?>"></div>
                                            <div class="col-sm-6 timeline-item" data-target="<?= $i ?>">
                                                <div class="row">
                                                    <div class="col-sm-11">
                                                        <div class="timeline-panel credits anim animate fadeInLeft credits-<?= $i ?>" data-value="2">
                                                            <ul class="timeline-panel-ul">
                                                                <div class="lefting-wrap">
                                                                    <li class="img-wraping"><a
                                                                                href="<?php the_permalink(); ?>/?chapter2"><?php the_post_thumbnail('img-responsive'); ?></a>
                                                                    </li>
                                                                </div>
                                                                <div class="righting-wrap">
                                                                    <li><?= the_title('<a href="' . esc_url(get_permalink()) . '/?chapter2" class="importo">', '</a>'); ?></li>
                                                                    <li><a href="<?=esc_url(get_permalink())?>/?chapter2" class="post-desc"><?= excerpt(25) ;?></a></li>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="finished text-center" style="display: none;">
                                                                    <i class="fas fa-check-double"></i> Post read
                                                                </div>
                                                            </ul>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $i++;
                                    endforeach;
                                    ?>

                                </div>
                            </div>
                        </div>
                        <script>
                            let tempArr = [<?= '"'.implode('","', $tmpArr2).'"' ?>];
                            for (var i = 0; i < tempArr.length; i++ ) {
                                $('.timeline-movement.post-'+tempArr[i]+' .finished').show();
                            }
                        </script>
                    <?php elseif (isset($_GET['chapter3'])): ?>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="text-center" style="width: 100%;">
                                    <h1>Chapter 3 - Didactics with the use of algorithmic and programming</h1>
                                </div>
                            </div>
                            <div class="row">
                                <a href="/handbook" class="btn btn-info">◄&nbsp; Back to Main Menu</a>
                                <div class="container">
                                    <?php if (isset($finishedPosts[3])) {
                                        $var1 = 100 / 7;
                                        $res = $var1 * (count($finishedPosts[3])-1);
                                    }
                                    ?>
                                    <h2 class="text-center">Your progress</h2>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:<?= $finishedPosts[3] ? $res : 0?>%">
                                            <span><?= $finishedPosts[3] ? round($res) : 0?>% Complete</span>
                                        </div>
                                    </div>
                                </div>

                                <div id="timeline">
                                    <?php
                                    global $post;
                                    $args = array('category' => '35', 'posts_per_page' => 9,);
                                    $myposts = get_posts($args);
                                    $i = 0;
                                    foreach ($myposts as $post): setup_postdata($post); ?>
                                        <div class="row timeline-movement post-<?= $i ?>">
                                            <div class="timeline-badge center-left badge-<?= $i ?>"></div>
                                            <div class="col-sm-6 timeline-item" data-target="<?= $i ?>">
                                                <div class="row">
                                                    <div class="col-sm-11">
                                                        <div class="timeline-panel credits anim animate fadeInLeft credits-<?= $i ?>" data-value="3">
                                                            <ul class="timeline-panel-ul">
                                                                <div class="lefting-wrap">
                                                                    <li class="img-wraping"><a
                                                                                href="<?php the_permalink(); ?>/?chapter3"><?php the_post_thumbnail('img-responsive'); ?></a>
                                                                    </li>
                                                                </div>
                                                                <div class="righting-wrap">
                                                                    <li><?= the_title('<a href="' . esc_url(get_permalink()) . '/?chapter3" class="importo">', '</a>'); ?></li>
                                                                    <li><a href="<?=esc_url(get_permalink())?>/?chapter3" class="post-desc"><?= excerpt(25) ;?></a></li>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="finished text-center" style="display: none;">
                                                                    <i class="fas fa-check-double"></i> Post read
                                                                </div>
                                                            </ul>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $i++;
                                    endforeach;
                                    ?>

                                </div>
                            </div>
                        </div>
                        <script>
                            let tempArr = [<?= '"'.implode('","', $tmpArr3).'"' ?>];
                            for (var i = 0; i < tempArr.length; i++ ) {
                                $('.timeline-movement.post-'+tempArr[i]+' .finished').show();
                            }
                        </script>
                    <?php endif; ?>

                    <?php if($testCompleted == 1): ?>
                        <script>
                            $('#testBefore').hide();
                            $('#testAfter').show();
                            $('#chapters-buttons').show(400);
                        </script>
                    <?php endif;?>

                </article>

            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .container -->
<script>
    $('.timeline-panel').each(function(index) {
        $(this).on('click', function() {
            var currentChapter = $(this).attr('data-value');
            // console.log(currentChapter);
            $.ajax({
                url: '/wp-content/themes/codeit/comments_likes_data.php',
                type: 'POST',
                dataType: 'json',
                cache: false,
                data: {
                    'chapter': currentChapter,
                    'currentDiv': index
                }
            });
        });
    });

    $('#notLogin').on('click', function(e) {
        e.preventDefault();
        swal({
            title: "To solve the test, you have to log in!",
            type: "info",
            button: "Ok!",
        })
    })
</script>

<?php
get_footer();
