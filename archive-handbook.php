<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 13.08.2018
 * Time: 14:51
 */

/**
 * Template Name: Featured Article
 * Template Post Type: post
 */

get_header(); ?>
    <div class="container" id="customPosts">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <?php
                /* Start the Loop */
                while ( have_posts() ) :
                    the_post();
                    ?>
                    <div class="row">
                        <div class="col-md-2">
                            <?php
                            if (isset($_GET['chapter1'])):?>
                                <a href="/handbook/?chapter1" class="btn btn-info">◄&nbsp; Back to Timeline</a>
                            <?php elseif (isset($_GET['chapter2'])): ?>
                                <a href="/handbook/?chapter2" class="btn btn-info">◄&nbsp; Back to Timeline</a>
                            <?php elseif (isset($_GET['chapter3'])): ?>
                                <a href="/handbook/?chapter3" class="btn btn-info">◄&nbsp; Back to Timeline</a>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <?php $orig_post = $post;
                            global $post;
                            $categories = get_the_category($post->ID);
                            if ($categories) {
                                $category_ids = array();
                                foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

                                $args=array(
                                    'category__in' => $category_ids,
                                    'post__not_in' => array($post->ID),
                                    'posts_per_page'=> 16, // Number of related posts that will be shown.
                                    'caller_get_posts'=>1
                                );

                                $my_query = new wp_query( $args );
                                if( $my_query->have_posts() ) {
                                    echo '<div id="related_posts"><h3>Chapter Content</h3><ul>';
                                    while( $my_query->have_posts() ):
                                        $my_query->the_post();?>

                                        <li><div class="relatedcontent">
                                                <?php
                                                if (isset($_GET['chapter1'])):?>
                                                    <h6><a href="<?= the_permalink()?>/?chapter1" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
                                                <?php elseif (isset($_GET['chapter2'])): ?>
                                                    <h6><a href="<?= the_permalink()?>/?chapter2" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
                                                <?php elseif (isset($_GET['chapter3'])): ?>
                                                    <h6><a href="<?= the_permalink()?>/?chapter3" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h6>
                                                <?php endif;?>
                                            </div>
                                        </li>
                                    <?php
                                    endwhile;
                                    echo '</ul></div>';
                                }
                            }
                            $post = $orig_post;
                            wp_reset_query(); ?>
                        </div>

                        <div class="col-md-9">
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="margin-top: 25px;padding: 15px;">
                                <header class="entry-header">
                                    <?php
                                    if ( is_single() ) {
                                        the_title( '<h1 class="entry-title">', '</h1>' );
                                    } elseif ( is_front_page() && is_home() ) {
                                        the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
                                    } else {
                                        the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                                    }
                                    ?>
                                </header><!-- .entry-header -->

                                <div class="entry-content">
                                    <?php
                                    /* translators: %s: Name of current post */
                                    the_content(
                                        sprintf(
                                            __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
                                            get_the_title()
                                        )
                                    );
                                    wp_link_pages(
                                        array(
                                            'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
                                            'after'       => '</div>',
                                            'link_before' => '<span class="page-number">',
                                            'link_after'  => '</span>',
                                        )
                                    );
                                    ?>
                                </div><!-- .entry-content -->
                            </article><!-- #post-## -->
                        </div>

                    </div>

                <?php
                endwhile; // End of the loop.
                ?>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .container -->
<?php
get_footer();