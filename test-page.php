<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 19.10.2018
 * Time: 09:22
 * Template name: Test page
 */

global $wpdb;
$pageTitle = '';
$query = 'SELECT `question`, `answers` FROM `co_questions`';

$result = $wpdb->get_results($query, OBJECT);

$userId = get_current_user_id();

get_header(); ?>
<style>
    .codeit_question, .points-div {
        border: 1px solid #888888;
        border-radius: .5rem;
        padding: 15px;
        box-shadow: 0 16px 38px -12px rgba(0,0,0,.56), 0 4px 25px 0 rgba(0,0,0,.12), 0 8px 10px -5px rgba(0,0,0,.2);
        margin-bottom: 35px;
    }
</style>
    <div class="container-fluid" id="lesson-plans">
        <div id="primary" class="content-area" style="max-width: 850px;">
            <main id="main" class="site-main" role="main">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php if(isset($_GET['before'])):?>
                    <h1 class="text-center" style="margin-bottom: 50px;">Check your knowledge before reading handbook.</h1>
                    <?php
                    $i = 1;
                    foreach ($result as $res) {
                        echo '<div class="codeit_question" id="div_q' . $i . '" style="display:none;">';
                        echo '<div class="text-center" style="font-size:2.25em;margin:1em;">Question ' . $i . ' of 30</div>';
                        echo '<p class="text-center" style="font-size:1.5em;">' . $res->question . '</p>';
                        $tmpArray = unserialize($res->answers);

                        foreach ($tmpArray as $key => $val) :?>
                            <label>
                                <div class="input-group">

                                    <div class="input-group-prepend" style="float: left;">
                                        <div class="input-group-text">
                                            <?= '<input class="ans" type="radio" name="q' . $i . '" value="' . $key . '" id="qid_' . $i . '_' . $key . '" data-value="' . $val[1] . '">' ?>
                                        </div>
                                    </div>
                                    <?= '<div style="padding: 5px;">' . $val[0] . '</div>' ?>
                                </div>
                            </label>
                        <?php endforeach;

                        echo '<div style="padding:2em 0 4em 0" class="row">
                                <div class="span3 col-md-3"><a id="button" class="btn btn-secondary prev_btn" data-question="' . $i . '" href="#"><i class="fas fa-caret-left"></i> previous</a></div>
                                <div class="span4 col-md-offset-2 col-md-7 text-right"><a id="button" data-question="' . $i . '" class="btn btn-success next_btn" href="#"> next <i class="fas fa-caret-right"></i></a>
                                <br /><span class="notfilled" style="color:red;font-size: 0.8em;display:none">Question has not been answered!</span>
                                </div>
                              </div>';

                        echo '</div>';
                        $i++;
                    }?>

                        <div class="summary-screen" style="display: none">
                            <div class="points-div"></div>
                            <div class="row justify-content-center" >
                                <div class="col-md-2">
                                    <button class="btn btn-primary" id="sendTest">◄&nbsp; Back to Handbook</button>
                                </div>
                            </div>
                        </div>
                    <?php elseif (isset($_GET['after'])):?>
                        <div class="row">
                            <div class="col-md-4">
                                <a href="/handbook" class="btn btn-info">◄&nbsp; Back to handbook</a>
                            </div>
                        </div>
                        <h1 class="text-center" style="margin-bottom: 50px;">Check your knowledge before reading handbook.</h1>
                        <?php
                        $i = 1;
                        foreach ($result as $res) {
                            echo '<div class="codeit_question" id="div_q' . $i . '" style="display:none;">';
                            echo '<div class="text-center" style="font-size:2.25em;margin:1em;">Question ' . $i . ' of 30</div>';
                            echo '<p class="text-center" style="font-size:1.5em;">' . $res->question . '</p>';
                            $tmpArray = unserialize($res->answers);

                            foreach ($tmpArray as $key => $val) :?>
                                <label>
                                    <div class="input-group">

                                        <div class="input-group-prepend" style="float: left;">
                                            <div class="input-group-text">
                                                <?= '<input class="ans" type="radio" name="q' . $i . '" value="' . $key . '" id="qid_' . $i . '_' . $key . '" data-value="' . $val[1] . '">' ?>
                                            </div>
                                        </div>
                                        <?= '<div style="padding: 5px;">' . $val[0] . '</div>' ?>
                                    </div>
                                </label>
                            <?php endforeach;

                            echo '<div style="padding:2em 0 4em 0" class="row">
                                <div class="span3 col-md-3"><a id="button" class="btn btn-secondary prev_btn" data-question="' . $i . '" href="#"><i class="fas fa-caret-left"></i> previous</a></div>
                                <div class="span4 col-md-offset-2 col-md-7 text-right"><a id="button" data-question="' . $i . '" class="btn btn-success next_btn" href="#"> next <i class="fas fa-caret-right"></i></a>
                                <br /><span class="notfilled" style="color:red;font-size: 0.8em;display:none">Question has not been answered!</span>
                                </div>
                              </div>';

                            echo '</div>';
                            $i++;
                        }?>

                        <div class="summary-screen" style="display: none">
                            <div class="points-div"></div>
                            <div class="row justify-content-center" >
                                <div class="col-md-2">
                                    <a href="/handbook" class="btn btn-primary" id="sendAfter">◄&nbsp; Back to Handbook</a>
                                </div>
                            </div>
                        </div>
                    <?php endif;?>

                </article>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .container -->
    <script>
        (function ($) {
            $('#div_q1').show();
            $('#div_q1 a.prev_btn').hide();
            jQuery('#div_q30 a.next_btn').text('Finish');

            let points = 0;

            $('.codeit_question a').on('click', function (e) {
                e.preventDefault();
                let current = parseInt($(this).attr('data-question'));
                $('#div_q' + current + ' .notfilled').hide();
                answersActions(current);
                if ($(this).hasClass('next_btn') && current !== 30) {
                    if ($('#div_q' + current + ' input[type="radio"]:checked').size() === 0) {
                        $('#div_q' + current + ' .notfilled').show(400);
                        return;
                    }
                    $('#div_q' + current).hide();
                    $('#div_q' + (current + 1)).show(400);

                }
                if ($(this).hasClass('prev_btn') && current !== 1) {
                    $('#div_q' + current).hide();
                    $('#div_q' + (current - 1)).show(400);
                }
                if ($(this).hasClass('next_btn') && current === 30) {
                    if ($('#div_q' + current + ' input[type="radio"]:checked').size() === 0) {
                        $('#div_q' + current + ' .notfilled').show(400);
                        return;
                    }
                    $('#div_q30').hide();
                    $('.summary-screen').show();
                    let score = points / 30 * 100;
                    $('.points-div').html('<p class="text-center" style="font-size: 1.4em;">Your score is ' + Math.round(score, 2) + ' %</p>');
                }
            });

            function answersActions(index) {
                let inputVal = $('#div_q' + index + ' input[name="q' + index + '"]:checked').attr('data-value');
                if (inputVal === "true") {
                    points += 1;
                }
            }

            $('#sendTest').on('click', function(e) {
                e.preventDefault();
                $.ajax({
                    url: '/wp-content/themes/codeit/comments_likes_data.php',
                    type: 'POST',
                    dataType: 'json',
                    cache: false,
                    data: {
                        'test_response': 1,
                        'user_id': '<?=$userId?>'
                    },
                    success: function (response) {
                        if (response === 1) {
                            window.location.href = '/handbook';
                        }
                    },
                });
            });

        }(jQuery));
    </script>
<?php
get_footer();
