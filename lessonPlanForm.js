(function ($) {
    let highestBox = 0;
    $('.lesson-card-title').each(function () {
        if ($(this).height() > highestBox) {
            highestBox = $(this).height();
        }
    });
    $('.lesson-card-title').height(highestBox);

    var addLessonPlan = $('#addLessonPlan');
    var backToPlansList = $('#backToPlansList');

    addLessonPlan.on('click', function (e) {
        e.preventDefault();
        $('#lesson').hide();
        $('#newLessonPlan').show(400);
    });

    backToPlansList.on('click', function (e) {
        e.preventDefault();
        $('#newLessonPlan').hide();
        $('#lesson').show(400);

    });

    var dataForm = $('#addPlan');
    dataForm.on('submit', function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            url: '/wp-content/themes/codeit/new-lesson-data.php',
            type: 'POST',
            data: formData,
            success: function () {
                location.reload();
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    //Learning Outcome part
    var addKnowledge = $('#add-knowledge');
    addKnowledge.on('click', function (e) {
        e.preventDefault();
        $('.learningOutcomes .knowledge').append('<input type="text" name="knowledge[]" class="input-knowledge"/>')
    });
    var removeKnowledge = $('#remove-knowledge');
    removeKnowledge.on('click', function (e) {
        e.preventDefault();
        $('.learningOutcomes .knowledge input').last().remove();
    });

    var addSkill = $('#add-skill');
    addSkill.on('click', function (e) {
        e.preventDefault();
        $('.learningOutcomes .skills').append('<input type="text" name="skills[]" class="input-skills"/>')
    });
    var removeSkill = $('#remove-skill');
    removeSkill.on('click', function (e) {
        e.preventDefault();
        $('.learningOutcomes .skills input').last().remove();
    });

    //Methods
    var addMethod = $('#add-method');
    addMethod.on('click', function (e) {
        e.preventDefault();
        $('.methods .method').append('<input type="text" name="method[]" class="input-method"/>')
    });
    var removeMethod = $('#remove-method');
    removeMethod.on('click', function (e) {
        e.preventDefault();
        $('.methods .method input').last().remove();
    });

    //Didactic materials
    var addDidactic = $('#add-didactic');
    addDidactic.on('click', function (e) {
        e.preventDefault();
        $('.didactics .didactic').append('<input type="text" name="didactic[]" class="input-didactic"/>')
    });
    var removeDidactic = $('#remove-didactic');
    removeDidactic.on('click', function (e) {
        e.preventDefault();
        $('.didactics .didactic input').last().remove();
    });

    //Lesson course
    var addIntroduction = $('#add-introduction');
    addIntroduction.on('click', function (e) {
        e.preventDefault();
        $('.course .introduction').append('<input type="text" name="introduction[]" class="input-introduction"/>')
    });
    var removeIntroduction = $('#remove-introduction');
    removeIntroduction.on('click', function (e) {
        e.preventDefault();
        $('.course .introduction input').last().remove();
    });
    var addCore = $('#add-core-part');
    addCore.on('click', function (e) {
        e.preventDefault();
        $('.course .core-part').append('<input type="text" name="core-part[]" class="input-core-part"/>')
    });
    var removeCore = $('#remove-core-part');
    removeCore.on('click', function (e) {
        e.preventDefault();
        $('.course .core-part input').last().remove();
    });
    var addSummary = $('#add-summary');
    addSummary.on('click', function (e) {
        e.preventDefault();
        $('.course .summary').append('<input type="text" name="summary[]" class="input-summary"/>')
    });
    var removeSummary = $('#remove-summary');
    removeSummary.on('click', function (e) {
        e.preventDefault();
        $('.course .summary input').last().remove();
    });
    var addAttitude = $('#add-attitude');
    addAttitude.on('click', function (e) {
        e.preventDefault();
        $('.learningOutcomes .attitudes').append('<input type="text" name="attitudes[]" class="input-attitudes"/>')
    });
    var removeAttitude = $('#remove-attitude');
    removeAttitude.on('click', function (e) {
        e.preventDefault();
        $('.learningOutcomes .attitudes input').last().remove();
    });


})(jQuery);