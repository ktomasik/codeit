<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 18.09.2018
 * Time: 12:15
 */
/**
* Template Name: Lesson plan
* Template Post Type: post
*/

get_header(); ?>
    <div class="container" id="customPosts">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <?php
                /* Start the Loop */
                while ( have_posts() ) :
                    the_post();
                    ?>

                        <a href="/lesson-plans" class="btn btn-info">◄&nbsp; Back</a>

                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="margin-top: 25px;padding: 15px;">
                        <header class="entry-header">
                            <?php
                            if ( is_single() ) {
                                the_title( '<h1 class="entry-title text-center">', '</h1>' );
                            } elseif ( is_front_page() && is_home() ) {
                                the_title( '<h3 class="entry-title text-center"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
                            } else {
                                the_title( '<h2 class="entry-title text-center"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                            }
                            ?>
                        </header><!-- .entry-header -->

                        <div class="entry-content">
                            <?php
                            /* translators: %s: Name of current post */
                            the_content(
                                sprintf(
                                    __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
                                    get_the_title()
                                )
                            );
                            wp_link_pages(
                                array(
                                    'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
                                    'after'       => '</div>',
                                    'link_before' => '<span class="page-number">',
                                    'link_after'  => '</span>',
                                )
                            );
                            ?>
                        </div><!-- .entry-content -->
                    </article><!-- #post-## -->
                <?php
                endwhile; // End of the loop.
                ?>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .container -->
    <script>
        (function ($) {
            let highestBox = 0;
            $('.learningOutcomes .card-text ul').each(function () {
                if ($(this).height() > highestBox) {
                    highestBox = $(this).height();
                }
            });
            $('.learningOutcomes .card-text ul').height(highestBox);

            $('.lessonCourse .card-text ul').each(function () {
                if ($(this).height() > highestBox) {
                    highestBox = $(this).height();
                }
            });
            $('.lessonCourse .card-text ul').height(highestBox);

        })(jQuery);
    </script>
<?php
get_footer();