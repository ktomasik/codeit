<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 16.11.2018
 * Time: 15:29
 */

require_once('../../../wp-load.php');

//print_r($_POST);
global $wpdb;
if (isset($_POST['like'])) {
    $like = $_POST['like'];
    $id = $_POST['id'];
    $wpdb->update('co_lesson_plans', array('like' => $like), array('id' => $id));

    echo true;
}
if (isset($_POST['dislike'])) {
    $disLike = $_POST['dislike'];
    $id = $_POST['id'];
    $wpdb->update('co_lesson_plans', array('dislike' => $disLike), array('id' => $id));

    echo true;
}

if (isset($_POST['comment'])) {
    $comment = $_POST['comment'];
    $id = $_POST['id'];
    $wpdb->insert('co_plan_comments', array(
        'plan_id' => $id,
        'comment' => $comment
    ));

    echo true;
}

if (isset($_POST['test_response'])) {
    $test_response = $_POST['test_response'];
    $userId = $_POST['user_id'];

    $wpdb->insert('co_user_actions', array(
        'test_response' => $test_response,
        'user_id' => $userId
    ));

    echo true;
}

if (isset($_POST['views'])) {
    $views = $_POST['views'];
    $id = $_POST['id'];
    $wpdb->update('co_lesson_plans', array('views' => $views), array('id' => $id));

    echo true;
}

if (isset($_POST['currentDiv'])) {
    $userId = get_current_user_id();
    $haveMeta = get_user_meta($userId, 'progress', false);
    $metas = get_user_meta($userId, 'progress', true);
    $meta_value = [];
    $chapter = $_POST['chapter'];
    $currentDiv = $_POST['currentDiv'];
    if (!$haveMeta) {
        $meta_value[$chapter][] = $currentDiv;
        add_user_meta( $userId, 'progress', $meta_value);
    } else {
        if(array_key_exists($chapter, $metas) === true) {
            foreach ($metas as $meta => $value) {
                if (in_array($currentDiv, $value)) {
                    continue;
                } else {
                    $metas[$meta][] = $currentDiv;
                    update_user_meta( $userId, 'progress', $metas );
                }
            }
        } else {
            foreach ($metas as $meta => $value) {
                if($chapter != $meta) {
                    $metas[$chapter][] = $currentDiv;
                    update_user_meta($userId, 'progress', $metas);
                }
            }
        }

    }

}