(function ($) {

    /**********************Scroll Animation "START"************************************/
    $(document).ready(function () {
        var $animation_elements = $('.anim');
        var $window = $(window);

        function check_if_in_view() {
            var window_height = $window.height();
            var window_top_position = $window.scrollTop();
            var window_bottom_position = (window_top_position + window_height);

            $.each($animation_elements, function () {
                var $element = $(this);
                var element_height = $element.outerHeight();
                var element_top_position = $element.offset().top;
                var element_bottom_position = (element_top_position + element_height);

//check to see if this current container is within viewport
                if ((element_bottom_position >= window_top_position) &&
                    (element_top_position <= window_bottom_position)) {
                    $element.addClass('animated');
                } else {
                    $element.removeClass('animated');
                }
            });
        }

        $window.on('scroll resize', check_if_in_view);
        $window.trigger('scroll');
    });
    /**********************Scroll Animation "END"************************************/

    $('.timeline-movement').each(function (index) {
        if ($(this).find('.timeline-item').attr('data-target') % 2 !== 0) {
            $(this).find('.timeline-item').addClass('offset-sm-6');
            $(this).find('.timeline-item .col-sm-11').addClass('offset-sm-1');
            $(this).find('.timeline-item .timeline-panel').addClass('debits debits-' + index).removeClass('credits');
            $(this).find('.timeline-badge').addClass('center-right').removeClass('center-left')
        }
        $(this).find(".debits-" + index).each(function (i) {
            $(this).hover(function () {
                $(".center-right.badge-" + index).css("background-color", "#4997cd");
                $(".center-right.badge-" + index).text(index + 1);
            }, function () {
                $(" .center-right").css("background-color", "#fff");
            });
        });
        $(this).find(".credits-" + index).each(function (i) {
            $(this).hover(function () {
                $(".center-left.badge-" + index).css("background-color", "#4997cd");
                $(".center-left.badge-" + index).text(index + 1);
            }, function () {
                $(" .center-left.badge-" + index).css("background-color", "#fff");
            });
        });
    });

    /******Lesson plans filter************/
    $('a.sortSubject').on('click', function(e){
        e.preventDefault();
        var target = $(this).attr('data-value');
        $("#lesson .row .col-md-4").hide();
        $("#lesson .row ."+target).each(function() {
            $(this).show(400);
        })
    });

    $('a.resetBtn').on('click', function(e) {
        e.preventDefault();
        $("#lesson .row .col-md-4").show(400);
    });

    /***Views counter**/
    $('#lesson .col-md-4 a').each(function(){
        var id = $(this).attr('id');
        var view = $(this).attr('data-value');
        $(this).on('click', function(e) {
            e.preventDefault();

            $.ajax({
                url: '/wp-content/themes/codeit/comments_likes_data.php',
                type: 'POST',
                dataType: 'json',
                cache: false,
                data: {
                    'views': view,
                    'id': id
                },
                success: function (response) {
                    if (response === 1) {
                        window.location.href = '/lesson-plans?plan_id='+id;
                    }
                },
            });
        })
    });
}(jQuery));