<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 16.11.2018
 * Time: 12:19
 */
$planId = $_GET['plan_id'];
$singlePlan = getSinglePlan($planId);
$comments = getPlanComments($planId);

$singleKnowledge = unserialize($singlePlan->knowledge);
$singleSkills = unserialize($singlePlan->skills);
$singleAttitudes = unserialize($singlePlan->attitudes);
$singleMethods = unserialize($singlePlan->methods);
$singleMaterials = unserialize($singlePlan->didactic_materials);
$singleIntroduction = unserialize($singlePlan->introduction);
$singleCoreParts = unserialize($singlePlan->core_parts);
$singleSummary = unserialize($singlePlan->summary);

get_header(); ?>

    <div class="container-fluid" id="lesson-plans">
        <div id="primary" class="content-area" style="max-width: 850px;">
            <main id="main" class="site-main" role="main">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <a class="btn btn-success"
                       href="<?= site_url() ?>/lesson-plans">◄&nbsp; Back</a>
                    <header class="entry-header">
                        <h1 class="h1 text-center"><?= $singlePlan->title ?></h1>
                    </header>
                    <div class="row" id="basicInfo">
                        <div class="col-md-8">
                            <span>Duration of the lesson: <?= $singlePlan->time ?> h</span><br/>
                            <span>Grade: <?= $singlePlan->grade ?></span><br/><br/>
                        </div>
                        <div class="col-md-4">
                            <button id="downloadPlan" class="btn btn-info" style="float: right;">Download lesson plan
                            </button>
                        </div>
                    </div>
                    <div class="learningOutcomes">
                        <h3 style="font-weight: bold">I. Learning outcomes</h3>
                        <div class="row">
                            <?php if ($singleKnowledge[0] != '' && $singleSkills[0] != '' && $singleAttitudes[0] != ''): ?>
                                <div class="col-md-4">
                                    <div class="card-text">
                                        <p class="text-center"><u>Knowledge</u></p>
                                        <ul>
                                            <?php foreach ($singleKnowledge as $know): ?>
                                                <li><?= $know ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card-text">
                                        <p class="text-center"><u>Skills</u></p>
                                        <ul>
                                            <?php foreach ($singleSkills as $skill): ?>
                                                <li><?= $skill ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card-text">
                                        <p class="text-center"><u>Attitudes</u></p>
                                        <ul>
                                            <?php foreach ($singleAttitudes as $attitude): ?>
                                                <li><?= $attitude ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php elseif ($singleKnowledge[0] == ''): ?>
                                <div class="col-md-6">
                                    <div class="card-text">
                                        <p class="text-center"><u>Skills</u></p>
                                        <ul>
                                            <?php foreach ($singleSkills as $skill): ?>
                                                <li><?= $skill ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card-text">
                                        <p class="text-center"><u>Attitudes</u></p>
                                        <ul>
                                            <?php foreach ($singleAttitudes as $attitude): ?>
                                                <li><?= $attitude ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php elseif ($singleSkills[0] == ''): ?>
                                <div class="col-md-6">
                                    <div class="card-text">
                                        <p class="text-center"><u>Knowledge</u></p>
                                        <ul>
                                            <?php foreach ($singleKnowledge as $know): ?>
                                                <li><?= $know ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card-text">
                                        <p class="text-center"><u>Attitudes</u></p>
                                        <ul>
                                            <?php foreach ($singleAttitudes as $attitude): ?>
                                                <li><?= $attitude ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php elseif ($singleAttitudes[0] == ''): ?>
                                <div class="col-md-6">
                                    <div class="card-text">
                                        <p class="text-center"><u>Knowledge</u></p>
                                        <ul>
                                            <?php foreach ($singleKnowledge as $know): ?>
                                                <li><?= $know ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card-text">
                                        <p class="text-center"><u>Skills</u></p>
                                        <ul>
                                            <?php foreach ($singleSkills as $skill): ?>
                                                <li><?= $skill ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="methods" style="margin-top: 1.5em;">
                        <h3 style="font-weight: bold">II. Methods and forms of work</h3>
                        <ul>
                            <?php foreach ($singleMethods as $method): ?>
                                <li><?= $method ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="didacticMaterials">
                        <h3 style="font-weight: bold">III. Didactic materials/resources</h3>
                        <div class="row">
                            <ul>
                                <?php foreach ($singleMaterials as $material): ?>
                                    <li><?= $material ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="lessonCourse">
                        <h3 style="font-weight: bold">IV. The course of the lesson</h3>
                        <div class="row">
                            <?php if ($singleIntroduction[0] != '' && $singleCoreParts[0] != '' && $singleSummary[0] != ''): ?>
                                <div class="col-md-4">
                                    <div class="card-text">
                                        <p class="text-center"><u>Introduction</u></p>
                                        <ul>
                                            <?php foreach ($singleIntroduction as $intro): ?>
                                                <li><?= $intro ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card-text">
                                        <p class="text-center"><u>Core part</u></p>
                                        <ul>
                                            <?php foreach ($singleCoreParts as $corePart): ?>
                                                <li><?= $corePart ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card-text">
                                        <p class="text-center"><u>Summary and evaluation</u></p>
                                        <ul>
                                            <?php foreach ($singleSummary as $sum): ?>
                                                <li><?= $sum ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php elseif ($singleIntroduction[0] == ''):?>
                                <div class="col-md-6">
                                    <div class="card-text">
                                        <p class="text-center"><u>Core part</u></p>
                                        <ul>
                                            <?php foreach ($singleCoreParts as $corePart): ?>
                                                <li><?= $corePart ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card-text">
                                        <p class="text-center"><u>Summary and evaluation</u></p>
                                        <ul>
                                            <?php foreach ($singleSummary as $sum): ?>
                                                <li><?= $sum ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php elseif ($singleCoreParts[0] == ''):?>
                                <div class="col-md-6">
                                    <div class="card-text">
                                        <p class="text-center"><u>Introduction</u></p>
                                        <ul>
                                            <?php foreach ($singleIntroduction as $intro): ?>
                                                <li><?= $intro ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card-text">
                                        <p class="text-center"><u>Summary and evaluation</u></p>
                                        <ul>
                                            <?php foreach ($singleSummary as $sum): ?>
                                                <li><?= $sum ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php elseif ($singleSummary[0] == ''):?>
                                <div class="col-md-6">
                                    <div class="card-text">
                                        <p class="text-center"><u>Introduction</u></p>
                                        <ul>
                                            <?php foreach ($singleIntroduction as $intro): ?>
                                                <li><?= $intro ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card-text">
                                        <p class="text-center"><u>Core part</u></p>
                                        <ul>
                                            <?php foreach ($singleCoreParts as $corePart): ?>
                                                <li><?= $corePart ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if ($singlePlan->appendices != ''): ?>
                        <div class="appendices" style="margin-top: 1.5em;">
                            <h3 style="font-weight: bold">V. Appendices</h3>
                            <div class="row">
                                <?php
                                $attach = explode(', ', $singlePlan->appendices);

                                ?>

                                <ul style="list-style-type: none" class="ul">
                                    <?php foreach ($attach as $file): ?>
                                        <li>
                                            <a href="/wp-content/themes/codeit/lesson-attachments/<?= $planId ?>/<?= $file ?>"
                                               target="_blank"><i class="fa fa-download"></i> <?= $file ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="comments">
                        <h3 style="font-weight: bold">VI. Comments</h3>
                        <div class="row" style="margin-bottom: 1.5em">
                            <div class="col-md-9">
                                <form method="post" id="newComment">
                                    <div class="form-group">
                                        <textarea class="form-control" title="comment" name="comment"></textarea>
                                        <input type="hidden" name="id" value="<?= $singlePlan->id ?>"/>
                                    </div>

                                </form>
                            </div>
                            <div class="col-md-3 align-self-end"><a href="#" id="like"
                                                                    style="float: left; color: green;"
                                                                    data-value="<?= intval($singlePlan->like) + 1 ?>">
                                    <i class="far fa-thumbs-up fa-2x"> <?= $singlePlan->like ?></i></a>
                                <a href="#" id="dislike" style="float: right; color: red;"
                                   data-value="<?= intval($singlePlan->dislike) + 1 ?>">
                                    <i class="far fa-thumbs-down fa-2x"> <?= $singlePlan->dislike ?></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <button class="btn btn-success" type="submit" id="addComment">Add new comment</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div>
                                    <?php foreach ($comments as $comment): ?>
                                        <p style="border-bottom: 1px solid #00000036"><?= $comment->comment ?></p>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </main>
        </div>
    </div>
    <script>
        $('.learningOutcomes .card-text').matchHeight();
        $('.lessonCourse .card-text').matchHeight();

        var like = $('#like').attr('data-value');
        var id = '<?=$singlePlan->id?>';
        $('#like').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/wp-content/themes/codeit/comments_likes_data.php',
                type: 'POST',
                dataType: 'json',
                cache: false,
                data: {
                    'like': like,
                    'id': id
                },
                success: function (response) {
                    if (response === 1) {
                        location.reload();
                    }
                },
            });
        });

        var dislike = $('#dislike').attr('data-value');
        $('#dislike').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/wp-content/themes/codeit/comments_likes_data.php',
                type: 'POST',
                dataType: 'json',
                cache: false,
                data: {
                    'dislike': dislike,
                    'id': id
                },
                success: function (response) {
                    if (response === 1) {
                        location.reload();
                    }
                },
            });
        });

        var comment = $('#newComment');
        $('#addComment').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/wp-content/themes/codeit/comments_likes_data.php',
                type: 'POST',
                dataType: 'json',
                cache: false,
                data: comment.serialize(),
                success: function (response) {
                    if (response === 1) {
                        location.reload();
                    }
                },
            });
        });

    </script>
<?php
get_footer();
